var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var dotenv = require('dotenv').config();
var MongoClient = require('mongodb').MongoClient;

/**
 * the database should be created only once
 */
router.get('/createDB', function(req,res){
    res.send("Database created!")
    MongoClient.connect(process.env.url, function(err, db) {
        if (err) throw err
        var dbo = db.db(process.env.dbName)
        dbo.createCollection("UserInfo", function(err, res) {
          if (err) throw err
          console.log("Collection created!")
          db.close()
        })
    })
})

router.post('/initDB', function(req,res){
    res.send("Initialise the database")
    var Params = getMockData();
    MongoClient.connect(process.env.url,function(error,db){
        if(error)throw error
        var dbo = db.db(process.env.dbName)
        dbo.collection("UserInfo").insertMany(Params, function(err, res) {
            if (err) throw err
            console.log("Inserted "+res.insertedCount+" items")
            db.close()
          })
    })
})

router.post('/testAdd',function(req,res){
    res.send("sent")
    console.log(JSON.stringify(req.body[0].lastName));
    console.log(typeof(JSON.stringify(req.body[0].lastName)));
})

router.post('/add', function(req,res){
    res.send("Added")
    var arrayParams = JSON.parse(JSON.stringify(req.body))
    console.log(arrayParams)
    MongoClient.connect(process.env.url,function(error,db){
        if(error)throw error
        var dbo = db.db(process.env.dbName)
        dbo.collection("UserInfo").insertMany(arrayParams, function(err, res) {
            if (err) throw err
            console.log("Inserted "+res.insertedCount+" items")
            db.close()
          })
    })
})

/**
 * 
 * This method is for search user according to the ContactID
 * @params id
 * @returns json object of the user
 */
//findOne
router.get('/search/:id', function(req, res){
    res.send("Finding...")
    MongoClient.connect(process.env.url, function(err, db) {
        if (err) throw err
        var dbo = db.db(process.env.dbName)
        var query = { contactID: parseInt(req.params.id)}
        //var query = { contactID: 1}
        dbo.collection("UserInfo").findOne((query),function(err, result) {
            if (err) throw err
            console.log(result)
            db.close()
          })
    })
})
/**
 * 
 * This method is for getting data from database and display it in frontend
 */
router.get('/listAll', function(req,res){
    res.send("Getting...")
    MongoClient.connect(process.env.url, function(err, db) {
        if (err) throw err
        var dbo = db.db(process.env.dbName)
        dbo.collection("UserInfo").find({}).toArray(function(err, result) {
            if (err) throw err
            console.log(result)
            db.close()
          })
    })
})

router.delete('/delete/:id',function(req,res){
    res.send("Deleting...")
    MongoClient.connect(process.env.url, function(err, db) {
        if (err) throw err
        var dbo = db.db(process.env.dbName)
        //Delete one item
        var query = { contactID: parseInt(req.params.id)}
        dbo.collection("UserInfo").deleteOne(query, function(err, result) {
          if (err) throw err
          console.log("ID:" + parseInt(req.params.id) + " is deleted")
          db.close();
        })
    })
})

router.put('/update/:name/:newName',function(req,res){
    res.send("Updating")
    MongoClient.connect(process.env.url, function(err, db) {
        if (err) throw err
        var dbo = db.db(process.env.dbName)
        //console.log(req.params.name)
        var myquery = { firstName: req.params.name};
        var newvalues = {$set: {lastName: req.params.newName} }
        dbo.collection("UserInfo").updateMany(myquery, newvalues, function(err, res) {
            if (err) throw err
            console.log(res.result.nModified + " document(s) updated")
            db.close();
        })
    })
})

function getMockData(){
    return [ 
        { 
            contactID : 001,
            firstName: "Book",
            lastName:"Cat",
            gender:"M",
            email:"abc@gmail.com",
            mobile:"090"
        },
        {
            contactID: 002,
            firstName: "Koko", 
            lastName:"Coco",
            gender:"M",
            email:"ff@gmail.com",
            mobile:"000"
        }
      ];
}
module.exports = router;
var express = require('express');
var MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');
var dotenv = require('dotenv').config();
var Contacts = require('./BackEnd/Controller/Contacts/contacts-controller')

var url = process.env.url;
const app = express()

app.use(bodyParser.urlencoded({ extended: true}))
app.use(bodyParser.json())
app.use('/contacts', Contacts);

MongoClient.connect(url,{ useNewUrlParser: true }, function(err, db) {
    if (err) throw err
    console.log("Connected Successfully");
})
app.listen(process.env.port);

console.log(`listening to port : ${process.env.port}`)